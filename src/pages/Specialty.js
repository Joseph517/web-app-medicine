import React, { useEffect, useState } from "react";
import {
  Table,
  Button,
  Modal,
  Form,
  Select,
  DatePicker,
  Switch,
  Input,
} from "antd";
const { Option } = Select;

const Specialty = () => {
  const [specialy, setSpecialty] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [dataToEdit, setDataToEdit] = useState(null);
  const [isEdit, setIsEdit] = useState(false);

  const columns = [
    {
      title: "Name specialty",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Description",
      dataIndex: "description",
    },
    {
      title: "dateRegister",
      dataIndex: "dateRegister",
      key: "dateRegister",
    },
    {
      title: "dateModification",
      dataIndex: "dateModification",
      key: "dateModification",
    },
    {
      title: "UserRegister",
      render: () => <span>lsv</span>,
      dataIndex: "UserRegister",
      key: "UserRegister",
    },
    {
      title: "UserModification",
      render: () => <span>lsv</span>,

      dataIndex: "UserModification",
      key: "UserModification",
    },

    //campo borrar
    {
      title: "Delete",
      dataIndex: "",
      key: "x",
      render: (value) => {
        return (
          <div>
            <a
              onClick={() => {
                deleteSpecialty(value.id);
              }}
            >
              Delete
            </a>
            <br />
            <a
              onClick={() => {
                handleEditSpecialty(value);
              }}
            >
              Edit
            </a>
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    loadSpecialty();
  }, []);

  //funcion crear especialidad
  const createSpecialty = async (formData) => {
    const url = "http://localhost:8000/api-medicine/specialty/";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    const responseSpecialty = await response.json();
    loadSpecialty();
  };

  //funcion editar
  const editSpecialty = async (formData) => {
    const url = `http://localhost:8000/api-medicine/specialty/${dataToEdit.id}/`;
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    loadSpecialty(formData);
    setIsEdit(false);
    setDataToEdit(null);
  };

  //funcion click editar
  const handleEditSpecialty = (dataSpecialty) => {
    setIsEdit(true);
    setIsModalVisible(true);
    setDataToEdit(dataSpecialty);
  };

  //funcion setear especialidad
  const loadSpecialty = () => {
    fetch("http://localhost:8000/api-medicine/specialty/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setSpecialty(data);
      });
  };

  const deleteSpecialty = async (id) => {
    const url = `http://localhost:8000/api-medicine/specialty/${id}/`;
    const response = await fetch(url, {
      method: "DELETE",
    });
    loadSpecialty();
  };

  const onFinish = (formData) => {
    //validar acitve
    if (!formData.active) {
      formData.active = false;
    }
    const dateRegister = formData.dateRegister.format("YYYY-MM-DD");
    formData.dateRegister = dateRegister;

    //editar
    if (isEdit) {
      editSpecialty(formData);
    } else {
      createSpecialty(formData);
    }
    setIsModalVisible(false);
  };

  return (
    <div className="w-100">
      <h2>Specialty</h2>

      <Button
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        Register Specialty
      </Button>

      <Table columns={columns} dataSource={specialy} />
      <Modal
        title="Register specialty"
        visible={isModalVisible}
        onCancel={() => {
          setIsModalVisible(false);
        }}
        onOk={() => {
          setIsModalVisible(false);
        }}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Name"
            name="name"
            rules={[
              {
                required: true,
                message: "Please input your last Name!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="description"
            name="description"
            rules={[
              {
                required: true,
                message: "Please input your last description!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="dateRegister"
            name="dateRegister"
            rules={[
              { required: true, message: "Please input your dateRegister!" },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="dateModification"
            name="dateModification"
            rules={[
              {
                required: true,
                message: "Please input your last dateModification!",
              },
            ]}
          >
            <DatePicker showTime />
          </Form.Item>

          <Form.Item
            label="userRegister"
            name="userRegister"
            rules={[
              { required: true, message: "Please input your userRegister!" },
            ]}
          >
            <Select>
              <Option value={"1"}>lsv</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="userModification"
            name="userModification"
            rules={[
              {
                required: true,
                message: "Please input your userModification!",
              },
            ]}
          >
            <Select>
              <Option value={"1"}>lsv</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="active"
            name="active"
            rules={[
              {
                required: false,
                message: "Please input your last active!",
              },
            ]}
          >
            <Switch />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default Specialty;
