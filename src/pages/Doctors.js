import React from "react";
import { useEffect, useState } from "react";
import {
  Table,
  Button,
  Modal,
  Form,
  Select,
  DatePicker,
  Switch,
  Input,
} from "antd";

const Doctors = () => {
  const [doctors, setDoctors] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [dataToEdit, setDataToEdit] = useState(null);
  const [isEdit, setIsEdit] = useState(false);

  //Columnas de la tabla
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Last name",
      dataIndex: "lastName",
      key: "lastName",
    },
    {
      title: "DNI",
      dataIndex: "dni",
      key: "dni",
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Sex",
      dataIndex: "sex",
      key: "sex",
    },
    {
      title: "numTuition",
      dataIndex: "numTuition",
      key: "numTuition",
    },
    {
      title: "Birthday date",
      dataIndex: "birthDate",
      key: "birthDate",
    },
    //campo borrar y editar
    {
      title: "Delete",
      dataIndex: "",
      key: "x",
      render: (value) => {
        return (
          <div>
            <a
              onClick={() => {
                deleteDoctor(value.id);
              }}
            >
              Delete
            </a>
            <br />
            <a
              onClick={() => {
                handleEditDoctor(value);
              }}
            >
              EDIT
            </a>
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    loadDoctors();
  }, []);

  //funcion crear doctor
  const createDoctor = async (formData) => {
    const url = "http://localhost:8000/api-medicine/doctor/";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    const responseDoctor = await response.json();
    loadDoctors();
  };

  //funcion editar doctor
  const editDoctor = async (formData) => {
    const url = `http://localhost:8000/api-medicine/doctor/${dataToEdit.id}/`;
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    loadDoctors();
    setIsEdit(false);
    setDataToEdit(null);
  };

  //funcion click editar
  const handleEditDoctor = (dataAppointment) => {
    setIsEdit(true);
    setIsModalVisible(true);
    setDataToEdit(dataAppointment);
  };

  // funcion setear lista de doctores
  const loadDoctors = () => {
    fetch("http://127.0.0.1:8000/api-medicine/doctor/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setDoctors(data);
      });
  };

  //borrar doctor
  const deleteDoctor = async (id) => {
    const url = `http://localhost:8000/api-medicine/doctor/${id}/`;
    const response = await fetch(url, {
      method: "DELETE",
    });
    loadDoctors();
  };

  const onFinish = (formData) => {
    //validar active
    if (!formData.active) {
      formData.active = false;
    }
    const birthDate = formData.birthDate.format("YYYY-MM-DD");
    const registerDate = formData.registerDate.format("YYYY-MM-DD");
    formData.birthDate = birthDate;
    formData.registerDate = registerDate;

    //editar
    if (isEdit) {
      editDoctor(formData);
    } else {
      createDoctor(formData);
    }
    setIsModalVisible(false);
  };

  return (
    <div className="w-100">
      <h2>Doctors</h2>
      <Button
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        Register doctor
      </Button>

      <Table columns={columns} dataSource={doctors} />
      <Modal
        title="Register Doctor"
        visible={isModalVisible}
        onCancel={() => {
          setIsModalVisible(false);
        }}
        onOk={() => {
          setIsModalVisible(false);
        }}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="NAME"
            name="name"
            rules={[{ required: true, message: "Please input your name!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="LAST NAME"
            name="lastName"
            rules={[
              { required: true, message: "Please input your last name!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="DNI"
            name="dni"
            rules={[{ required: true, message: "Please input your last dni!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="ADDREES"
            name="address"
            rules={[
              { required: true, message: "Please input your last address!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="CORREO"
            name="correo"
            rules={[
              { required: true, message: "Please input your last correo!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="PHONE"
            name="phone"
            rules={[{ required: true, message: "Please input your phone!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="SEX"
            name="sex"
            rules={[{ required: true, message: "Please input your last sex!" }]}
          >
            <Select>
              <Select.Option value="male">Male</Select.Option>
              <Select.Option value="female">Female</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="numTuition"
            name="numTuition"
            rules={[
              { required: true, message: "Please input your last numTuition!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="birthDate"
            name="birthDate"
            rules={[
              { required: true, message: "Please input your last birthDate!" },
            ]}
          >
            <DatePicker format="DD/MM/YYYY" />
          </Form.Item>

          <Form.Item
            label="registerDate"
            name="registerDate"
            rules={[
              {
                required: true,
                message: "Please input your last registerDate!",
              },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="active"
            name="active"
            rules={[
              {
                required: false,
                message: "Please input your last active!",
              },
            ]}
          >
            <Switch />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default Doctors;
