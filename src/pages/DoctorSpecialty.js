import React, { useEffect, useState } from "react";
import { Table, Button, Modal, Form, Select, DatePicker, Switch } from "antd";
const { Option } = Select;

const DoctorSpecialty = () => {
  const [doctorSpecialty, setDoctorSpecialty] = useState([]);
  const [doctors, setDoctors] = useState([]);
  const [specialty, setSpecialty] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [dataToEdit, setDataToEdit] = useState(null);
  const [isEdit, setIsEdit] = useState(false);

  //columnas de la tabla
  const columns = [
    {
      title: "Id Doctor",
      dataIndex: "idDoctor",
      key: "idDoctor",
    },
    {
      title: "Id Specialty",
      dataIndex: "idSpecialty",
      key: "idSpecialty",
    },
    {
      title: "dateRegister",
      dataIndex: "dateRegister",
      key: "dateRegister",
    },
    {
      title: "dateModification",
      dataIndex: "dateModification",
      key: "dateModification",
    },
    {
      title: "UserRegister",
      render: () => <span>lsv</span>,
      dataIndex: "UserRegister",
      key: "UserRegister",
    },
    {
      title: "UserModification",
      render: () => <span>lsv</span>,

      dataIndex: "UserModification",
      key: "UserModification",
    },

    {
      title: "Active",
      render: (value) => {
        if (value) {
          return <span>Active</span>;
        } else {
          return <span>Inactive</span>;
        }
      },
      dataIndex: "active",
      key: "active",
    },

    //campo borrar
    {
      title: "Delete",
      dataIndex: "",
      key: "x",
      render: (value) => {
        return (
          <div>
            <a
              onClick={() => {
                deleteDoctorSpecialty(value.id);
              }}
            >
              Delete
            </a>
            <br />
            <a
              onClick={() => {
                handleEditDoctorSpecialty(value);
              }}
            >
              Edit
            </a>
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    loadDoctorSpecialty();
    loadDoctors();
    loadSpecialty();
  }, []);

  //funcion crear una asignacion de especialidad a un doctor
  const createDoctorSpecialty = async (formData) => {
    const url = "http://localhost:8000/api-medicine/doctor-specialty/";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    const responseDoctorSpecialty = await response.json();
    loadDoctorSpecialty();
  };

  //funcion editar
  const editDoctorSpecialty = async (formData) => {
    const url = `http://localhost:8000/api-medicine/doctor-specialty/${dataToEdit.id}/`;
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    loadDoctorSpecialty();
    setIsEdit(false);
    setDataToEdit(null);
  };

  //funcion click editar
  const handleEditDoctorSpecialty = (dataDoctorSpecialty) => {
    setIsEdit(true);
    setIsModalVisible(true);
    setDataToEdit(dataDoctorSpecialty);
  };

  //funcion consultar doctores
  const loadDoctors = () => {
    fetch("http://localhost:8000/api-medicine/doctor/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setDoctors(data);
      });
  };

  //funcion consultar espcialidades
  const loadSpecialty = () => {
    fetch("http://localhost:8000/api-medicine/specialty/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setSpecialty(data);
      });
  };

  //funcion setear especialidad de doctores
  const loadDoctorSpecialty = () => {
    fetch("http://127.0.0.1:8000/api-medicine/doctor-specialty/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setDoctorSpecialty(data);
      });
  };

  //funcion borrar especialidades de doctor
  const deleteDoctorSpecialty = async (id) => {
    const url = `http://localhost:8000/api-medicine/doctor-specialty/${id}`;
    const response = await fetch(url, {
      method: "DELETE",
    });
    loadDoctorSpecialty();
  };

  const onFinish = (formData) => {
    //validar acitive
    if (!formData.active) {
      formData.active = false;
    }
    const dateRegister = formData.dateRegister.format("YYYY-MM-DD");
    const dateModification = formData.dateModification.format("YYYY-MM-DD");
    formData.dateModification = dateModification;
    formData.dateRegister = dateRegister;

    //editar
    if (isEdit) {
      editDoctorSpecialty(formData);
    } else {
      createDoctorSpecialty(formData);
    }
    setIsModalVisible(false);
  };
  return (
    <div className="w-100">
      <h2>DoctorSpecialty</h2>
      <Button
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        Register Doctor Specialty
      </Button>
      <Table columns={columns} dataSource={doctorSpecialty} />
      <Modal
        title="Register Doctor Specialty"
        visible={isModalVisible}
        onCancel={() => {
          setIsModalVisible(false);
        }}
        onOk={() => {
          setIsModalVisible(false);
        }}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="idDoctor"
            name="idDoctor"
            rules={[{ required: true, message: "Please input your idDoctor!" }]}
          >
            <Select>
              {doctors.map((doctors) => (
                <Option
                  value={doctors.id}
                >{`${doctors.name} ${doctors.lastName}`}</Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            label="idSpecialty"
            name="idSpecialty"
            rules={[
              { required: true, message: "Please input your idSpecialty!" },
            ]}
          >
            <Select>
              {specialty.map((specialty) => (
                <Option value={specialty.id}>{`${specialty.name}`}</Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            label="dateRegister"
            name="dateRegister"
            rules={[
              { required: true, message: "Please input your dateRegister!" },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="dateModification"
            name="dateModification"
            rules={[
              {
                required: true,
                message: "Please input your last dateModification!",
              },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="userRegister"
            name="userRegister"
            rules={[
              { required: true, message: "Please input your userRegister!" },
            ]}
          >
            <Select>
              <Option value={"1"}>lsv</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="userModification"
            name="userModification"
            rules={[
              {
                required: true,
                message: "Please input your userModification!",
              },
            ]}
          >
            <Select>
              <Option value={"1"}>lsv</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="active"
            name="active"
            rules={[
              {
                required: false,
                message: "Please input your last active!",
              },
            ]}
          >
            <Switch />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default DoctorSpecialty;
