import React, { useEffect, useState } from "react";
import { Table, Button, Modal, Form, Select, DatePicker, Switch } from "antd";
const { Option } = Select;

const Appointment = () => {
  const [appointment, setAppointment] = useState([]);
  const [doctors, setDoctors] = useState([]);
  const [patients, setPatients] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [dataToEdit, setDataToEdit] = useState(null);
  const [isEdit, setIsEdit] = useState(false);

  //columnas de la tabla
  const columns = [
    {
      title: "Id Doctor",
      dataIndex: "idDoctor",
      key: "idDoctor",
    },
    {
      title: "ID Patient",
      dataIndex: "idPatient",
      key: "idPatient",
    },
    {
      title: "Attention Date",
      dataIndex: "attentionDate",
      key: "attentionDate",
    },
    {
      title: "startAttentionDate",
      dataIndex: "startAttentionDate",
      key: "startAttentionDate",
    },
    {
      title: "endAttentionDate",
      dataIndex: "endAttentionDate",
      key: "endAttentionDate",
    },
    {
      title: "status",
      render: (value) => {
        if (value) {
          return <span>Active</span>;
        } else {
          return <span>Inactive</span>;
        }
      },

      dataIndex: "status",
      key: "status",
    },
    {
      title: "registerDate",
      dataIndex: "registerDate",
      key: "registerDate",
    },
    {
      title: "dateModification",
      dataIndex: "dateModification",
      key: "dateModification",
    },
    {
      title: "UserRegister",
      render: () => <span>lsv</span>,
      dataIndex: "UserRegister",
      key: "UserRegister",
    },
    {
      title: "UserModification",
      render: () => <span>lsv</span>,

      dataIndex: "UserModification",
      key: "UserModification",
    },
    //campo borrar y editar
    {
      title: "Delete",
      dataIndex: "",
      key: "x",
      render: (value) => {
        return (
          <div>
            <a
              onClick={() => {
                deleteAppointment(value.id);
              }}
            >
              Delete
            </a>
            <br />
            <a
              onClick={() => {
                handleEditAppointment(value);
              }}
            >
              EDIT
            </a>
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    loadAppointment();
    loadDoctors();
    loadPatient();
  }, []);

  //funcion crear cita
  const createAppointment = async (formData) => {
    const url = "http://localhost:8000/api-medicine/appointment/";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    loadAppointment();
  };

  //funcion editar
  const editAppointment = async (formData) => {
    const url = `http://localhost:8000/api-medicine/appointment/${dataToEdit.id}/`;
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    loadAppointment();
    setIsEdit(false);
    setDataToEdit(null);
  };

  //funcion click editar
  const handleEditAppointment = (dataAppointment) => {
    setIsEdit(true);
    setIsModalVisible(true);
    setDataToEdit(dataAppointment);
  };

  //funcion consultar doctores
  const loadDoctors = () => {
    fetch("http://localhost:8000/api-medicine/doctor/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setDoctors(data);
      });
  };

  //funcion consultar paciente
  const loadPatient = () => {
    fetch("http://localhost:8000/api-medicine/patient/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setPatients(data);
      });
  };

  //funcion setear citas
  const loadAppointment = () => {
    fetch("http://127.0.0.1:8000/api-medicine/appointment/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setAppointment(data);
      });
  };

  //funcion borrar  cita
  const deleteAppointment = async (id) => {
    const url = `http://localhost:8000/api-medicine/appointment/${id}`;
    const response = await fetch(url, {
      method: "DELETE",
    });
    loadAppointment();
  };

  //formato a la fecha
  const onFinish = (formData) => {
    //validar status
    if (!formData.status) {
      formData.status = false;
    }
    const attentionDate = formData.attentionDate.format("YYYY-MM-DD");
    formData.attentionDate = attentionDate;

    //editar
    if (isEdit) {
      editAppointment(formData);
    } else {
      createAppointment(formData);
    }
    setIsModalVisible(false);
  };
  return (
    <div className="w-100">
      <h2>Appointment</h2>
      <Button
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        Register Appointment
      </Button>

      <Table columns={columns} dataSource={appointment} />
      <Modal
        title="Create Appointment"
        visible={isModalVisible}
        onCancel={() => {
          setIsModalVisible(false);
        }}
        onOk={() => {
          setIsModalVisible(false);
        }}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="idDoctor"
            name="idDoctor"
            rules={[{ required: true, message: "Please input your idDoctor!" }]}
          >
            <Select>
              {doctors.map((doctors) => (
                <Option
                  key={doctors.id}
                  value={doctors.id}
                >{`${doctors.name} ${doctors.lastName}`}</Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            label="idPatient"
            name="idPatient"
            rules={[
              { required: true, message: "Please input your idPatient!" },
            ]}
          >
            <Select>
              {patients.map((patient) => (
                <Option
                  value={patient.id}
                >{`${patient.name} ${patient.lastName}`}</Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            label="attentionDate"
            name="attentionDate"
            rules={[
              {
                required: true,
                message: "Please input your last attentionDate!",
              },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="startAttentionDate"
            name="startAttentionDate"
            rules={[
              {
                required: true,
                message: "Please input your last startAttentionDate!",
              },
            ]}
          >
            <DatePicker showTime />
          </Form.Item>

          <Form.Item
            label="endAttentionDate"
            name="endAttentionDate"
            rules={[
              {
                required: true,
                message: "Please input your last endAttentionDate!",
              },
            ]}
          >
            <DatePicker showTime />
          </Form.Item>

          <Form.Item
            label="status"
            name="status"
            rules={[
              {
                required: false,
                message: "Please input your last status!",
              },
            ]}
          >
            <Switch />
          </Form.Item>

          <Form.Item
            label="registerDate"
            name="registerDate"
            rules={[
              {
                required: true,
                message: "Please input your last registerDate!",
              },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="userRegister"
            name="userRegister"
            rules={[
              { required: true, message: "Please input your userRegister!" },
            ]}
          >
            <Select>
              <Option value={"1"}>lsv</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="dateModification"
            name="dateModification"
            rules={[
              {
                required: true,
                message: "Please input your last dateModification!",
              },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="userModification"
            name="userModification"
            rules={[
              {
                required: true,
                message: "Please input your userModification!",
              },
            ]}
          >
            <Select>
              <Option value={"1"}>lsv</Option>
            </Select>
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default Appointment;
