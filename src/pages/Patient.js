import React from "react";
import { useEffect, useState } from "react";
import {
  Table,
  Button,
  Modal,
  Form,
  Select,
  DatePicker,
  Switch,
  Input,
} from "antd";

const Patient = () => {
  const [patients, setPatients] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);

  //columnas de la tabla
  const columns = [
    {
      title: "Name,",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Last name",
      dataIndex: "lastName",
      key: "lastName",
    },
    {
      title: "DNI",
      dataIndex: "dni",
      key: "dni",
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Sex",
      dataIndex: "sex",
      key: "sex",
    },
    {
      title: "Birthday date",
      dataIndex: "birthDate",
      key: "birthDate",
    },
    {
      title: "Delete",
      dataIndex: "",
      key: "x",
      render: (value) => {
        return (
          <a
            onClick={() => {
              deletePatient(value.id);
            }}
          >
            Delete
          </a>
        );
      },
    },
  ];

  useEffect(() => {
    loadPatient();
  }, []);

  //funcion crear paciente
  const createPatient = async (formData) => {
    const url = "http://localhost:8000/api-medicine/patient/";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    const responsePatient = await response.json();
    loadPatient();
  };

  //funcion setear lista de pacientes
  const loadPatient = () => {
    fetch("http://localhost:8000/api-medicine/patient/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setPatients(data);
      });
  };

  //funcion borrar paciente
  const deletePatient = async (id) => {
    const url = `http://localhost:8000/api-medicine/patient/${id}/`;
    const response = await fetch(url, {
      method: "DELETE",
    });
    loadPatient();
  };

  //
  const onFinish = (formData) => {
    const birthDate = formData.birthDate.format("YYYY-MM-DD");
    const registerDate = formData.registerDate.format("YYYY-MM-DD");
    //validar acitve
    if (!formData.active) {
      formData.active = false;
    }
    formData.birthDate = birthDate;
    formData.registerDate = registerDate;
    createPatient(formData);
    setIsModalVisible(false);
  };

  return (
    <div className="w-100">
      <h2>Patient</h2>
      <Button
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        Register Patient
      </Button>
      <Table columns={columns} dataSource={patients} />
      <Modal
        title="Register Patient"
        visible={isModalVisible}
        onCancel={() => {
          setIsModalVisible(false);
        }}
        onOk={() => {
          setIsModalVisible(false);
        }}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="NAME"
            name="name"
            rules={[{ required: true, message: "Please input your name!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="LAST NAME"
            name="lastName"
            rules={[
              { required: true, message: "Please input your last name!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="DNI"
            name="dni"
            rules={[{ required: true, message: "Please input your last dni!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="ADDREES"
            name="address"
            rules={[
              { required: true, message: "Please input your last address!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="CORREO"
            name="correo"
            rules={[
              { required: true, message: "Please input your last correo!" },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="PHONE"
            name="phone"
            rules={[{ required: true, message: "Please input your phone!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="SEX"
            name="sex"
            rules={[{ required: true, message: "Please input your last sex!" }]}
          >
            <Select>
              <Select.Option value="male">Male</Select.Option>
              <Select.Option value="female">Female</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="birthDate"
            name="birthDate"
            rules={[
              { required: true, message: "Please input your last birthDate!" },
            ]}
          >
            <DatePicker format="DD/MM/YYYY" />
          </Form.Item>

          <Form.Item
            label="registerDate"
            name="registerDate"
            rules={[
              {
                required: true,
                message: "Please input your last registerDate!",
              },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="active"
            name="active"
            rules={[
              {
                required: false,
                message: "Please input your last active!",
              },
            ]}
          >
            <Switch />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default Patient;
