import React, { useEffect, useState } from "react";
import { Table, Button, Modal, Form, Select, DatePicker, Switch } from "antd";
const { Option } = Select;

const Schedule = () => {
  const [schedules, setSchedules] = useState([]);
  const [doctors, setDoctors] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [dataToEdit, setDataToEdit] = useState(null);
  const [isEdit, setIsEdit] = useState(false);

  //columnas de la tabla
  const columns = [
    {
      title: "Id Doctor",
      dataIndex: "idDoctor",
      key: "idDoctor",
    },
    {
      title: "dateAttention",
      dataIndex: "dateAttention",
      key: "dateAttention",
    },
    {
      title: "startDateAttention",
      dataIndex: "startDateAttention",
      key: "startDateAttention",
    },
    {
      title: "endDateAttention",
      dataIndex: "endDateAttention",
      key: "endDateAttention",
    },
    {
      title: "Active",
      render: (value) => {
        if (value) {
          return <span>Active</span>;
        } else {
          return <span>Inactive</span>;
        }
      },
      dataIndex: "active",
      key: "active",
    },
    {
      title: "dateRegister",
      dataIndex: "dateRegister",
      key: "dateRegister",
    },
    {
      title: "UserRegister",
      render: () => <span>lsv</span>,
      dataIndex: "UserRegister",
      key: "UserRegister",
    },
    {
      title: "UserModification",
      render: () => <span>lsv</span>,

      dataIndex: "UserModification",
      key: "UserModification",
    },

    //campo borrar
    {
      title: "Delete",
      dataIndex: "",
      key: "x",
      render: (value) => {
        return (
          <div>
            <a
              onClick={() => {
                deleteSchedule(value.id);
              }}
            >
              Delete
            </a>
            <br />
            <a
              onClick={() => {
                handleEditSchedule(value);
              }}
            >
              Edit
            </a>
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    loadSchedule();
    loadDoctors();
  }, []);

  //funcion crear horario
  const createSchedule = async (formData) => {
    const url = "http://localhost:8000/api-medicine/shedule/";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    const responseSchedule = await response.json();
    loadSchedule();
  };

  //funcion editar
  const editSchedule = async (formData) => {
    const url = `http://localhost:8000/api-medicine/shedule/${dataToEdit.id}/`;
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    });
    loadSchedule();
    setIsEdit(false);
    setDataToEdit(null);
  };

  //funcion click editar
  const handleEditSchedule = async (dataSchedule) => {
    setIsEdit(true);
    setIsModalVisible(true);
    setDataToEdit(dataSchedule);
  };

  //funcion consultar doctores
  const loadDoctors = () => {
    fetch("http://localhost:8000/api-medicine/doctor/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setDoctors(data);
      });
  };

  //funcion setear horarios
  const loadSchedule = () => {
    fetch("http://localhost:8000/api-medicine/shedule/")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setSchedules(data);
      });
  };

  //funcion borrar horario
  const deleteSchedule = async (id) => {
    const url = `http://localhost:8000/api-medicine/shedule/${id}`;
    const response = await fetch(url, {
      method: "DELETE",
    });
    loadSchedule();
  };

  const onFinish = (formData) => {
    //validar active
    if (!formData.active) {
      formData.active = false;
    }
    //editar
    if (isEdit) {
      editSchedule(formData);
    } else {
      createSchedule(formData);
    }

    setIsModalVisible(false);
  };

  return (
    <div className="w-100">
      <h2>Schedule</h2>
      <Button
        onClick={() => {
          setIsModalVisible(true);
        }}
      >
        Register Schedule
      </Button>

      <Table columns={columns} dataSource={schedules} />
      <Modal
        title="Register Schedule"
        visible={isModalVisible}
        onCancel={() => {
          setIsModalVisible(false);
        }}
        onOk={() => {
          setIsModalVisible(false);
        }}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="idDoctor"
            name="idDoctor"
            rules={[{ required: true, message: "Please input your idDoctor!" }]}
          >
            <Select>
              {doctors.map((doctors) => (
                <Option
                  value={doctors.id}
                >{`${doctors.name} ${doctors.lastName}`}</Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            label="dateAttention"
            name="dateAttention"
            rules={[
              {
                required: true,
                message: "Please input your last dateAttention!",
              },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="startDateAttention"
            name="startDateAttention"
            rules={[
              {
                required: true,
                message: "Please input your last startDateAttention!",
              },
            ]}
          >
            <DatePicker showTime />
          </Form.Item>

          <Form.Item
            label="endDateAttention"
            name="endDateAttention"
            rules={[
              {
                required: true,
                message: "Please input your last endDateAttention!",
              },
            ]}
          >
            <DatePicker showTime />
          </Form.Item>

          <Form.Item
            label="active"
            name="active"
            rules={[
              {
                required: false,
                message: "Please input your last active!",
              },
            ]}
          >
            <Switch />
          </Form.Item>

          <Form.Item
            label="userRegister"
            name="userRegister"
            rules={[
              { required: true, message: "Please input your userRegister!" },
            ]}
          >
            <Select>
              <Option value={"1"}>lsv</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="dateModification"
            name="dateModification"
            rules={[
              {
                required: true,
                message: "Please input your last dateModification!",
              },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item
            label="userModification"
            name="userModification"
            rules={[
              {
                required: true,
                message: "Please input your userModification!",
              },
            ]}
          >
            <Select>
              <Option value={"1"}>lsv</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="dateRegister"
            name="dateRegister"
            rules={[
              { required: true, message: "Please input your dateRegister!" },
            ]}
          >
            <DatePicker />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default Schedule;
