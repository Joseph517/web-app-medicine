import React from "react";
import "../assets/style/Home.css";
import sanitas from "../assets/sanitas.png";
const Home = () => {
  return (
    <div className="home">
      <h2>Bienvenido</h2>
      <img width="100%" src={sanitas} />
    </div>
  );
};

export default Home;
