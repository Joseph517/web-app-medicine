import React from "react";
import { Link } from "react-router-dom";

const Sidebar = () => {
  return (
    <div>
      <nav id="sidebar">
        <div className="sidebar-header">
          <h3>EPS Sanitas</h3>
        </div>

        <ul className="list-unstyled components">
          <p>Opciones</p>
          <li className="active">
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/doctors">Doctors</Link>
          </li>
          <li>
            <Link to="/patient">Patient</Link>
          </li>
          <li>
            <Link to="/appointment">Appointment</Link>
          </li>
          <li>
            <Link to="/schedule">Schedule</Link>
          </li>
          <li>
            <Link to="/specialty">Specialty</Link>
          </li>
          <li>
            <Link to="/doctorSpecialty">DoctorSpecialty</Link>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Sidebar;
