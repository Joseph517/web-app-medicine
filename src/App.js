import "./App.css";
import { Routes, Route, BrowserRouter, Link } from "react-router-dom";
import Home from "./pages/Home";
import Doctors from "./pages/Doctors";
import Patient from "./pages/Patient";
import Appointment from "./pages/Appointment";
import Schedule from "./pages/Schedule";
import Specialty from "./pages/Specialty";
import DoctorSpecialty from "./pages/DoctorSpecialty";
import Sidebar from "./components/Sidebar";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Sidebar />
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/doctors" element={<Doctors />}></Route>
          <Route path="/patient" element={<Patient />}></Route>
          <Route path="/appointment" element={<Appointment />}></Route>
          <Route path="/schedule" element={<Schedule />}></Route>
          <Route path="/specialty" element={<Specialty />}></Route>
          <Route path="/doctorSpecialty" element={<DoctorSpecialty />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
